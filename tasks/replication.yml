---
# Master-node setup block
- block:

  - name: PostgreSQL Replication | Configure master server
    lineinfile:
      state: present
      backrefs: true
      dest: "{{ postgresql_config_path }}/postgresql.conf"
      regexp: "{{ item.regexp }}"
      line: "{{ item.line }}"
    with_items: "{{ postgresql_replication_conf }}"
    notify: restart postgresql
  
  - name: PostgreSQL Replication | Create replication user account
    postgresql_user:
      name: "{{ postgresql_replication_user }}"
      password: "{{ postgresql_replication_password }}"
      role_attr_flags: replication
    become: true
    become_user: postgres
  
  - name: PostgreSQL Replication | Add trust in pg_hba.conf
    lineinfile:
      state: present
      dest: "{{ postgresql_config_path }}/pg_hba.conf"
      regexp: "host.*replication.*{{ item }}/32.*trust"
      line: "host    replication    {{ postgresql_replication_user }}  {{ hostvars[item].ansible_default_ipv4.address }}/32 trust"
    with_items:
      - "{{ groups[postgresql_group_replica] }}"
    when:
      - groups[postgresql_group_replica] is iterable
      - postgresql_group_replica in groups

  - name: PostgreSQL Replication | Start PostgreSQL
    service:
      name: postgresql
      state: started

  when:
    - inventory_hostname in groups[postgresql_group_master]

- name: PostgreSQL Replication | check replication
  shell: ps -fu postgres | grep -c receiver
  register: postgresql_replication_check
  ignore_errors: true
  when:
    - inventory_hostname in groups[postgresql_group_replica]

# Replica-nodes setup block
- block:

  - name: PostgreSQL Replication | Stop PostgreSQL
    service:
      name: postgresql
      state: stopped
  
  - name: PostgreSQL Replication | Clear out data directory
    file:
      path: "{{ postgresql_data_directory }}/main"
      state: absent
  
  - name: PostgreSQL Replication | Create base backup
    command:  "pg_basebackup -R -X stream -c fast -D {{ postgresql_data_directory }}/main -h {{ hostvars[groups[postgresql_group_master][0]]['ansible_default_ipv4']['address'] }} -U {{ postgresql_replication_user }}"
    become: true
    become_user: postgres
    notify: "start postgresql"

  - name: PostgreSQL Replication | Enable hot standby
    lineinfile:
      state: present
      backup: true
      dest: "{{ postgresql_config_path }}/postgresql.conf"
      regexp: '^#?hot_standby = \w+(\s+#.*)'
      line: 'hot_standby = yes\1'
      backrefs: true
    become: true
    become_user: postgres
    when: postgresql_hotstandby
  
  when: 
    - inventory_hostname in groups[postgresql_group_replica]
    - postgresql_replication_check.stdout == '0'
